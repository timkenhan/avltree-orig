#include "avl_tree.h"
#include <iostream>
#define M 15
using namespace std;
int main () {
	avl_tree T;
	int I[15] = {29, 30, 15, 26, 7, 8, 91, 33, 57, 46, 77, 13, 24, 69, 10};
	for (int i = 0; i < M; ++i)
		T.insert(I[i]);
	//T.insert(I[M], 0);
	T.remove(57);
	T.remove(30);
	T.remove(29);
	T.remove(77);
	T.remove(15);
	T.remove(8);
	return 0;
}

// To-Do:
// CASE 1 only occur once. No avoid recursion on this case
// Sibling color check can be omitted after case 4
// New is not needed after case 1 (removal)
// return false instead of recalling sort function (ins case 3 & rem case 3)
