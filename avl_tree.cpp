// A Simple AVL Tree Implementation
// by Timothy K Handojo
/* Description:
 * This is a barebone implementation of Self-Balancing Binary Tree.
 * This base class is meant to be employed into specialized objects
 * appropriately when needed.
 */

#include "avl_tree.h"
#include <cstdlib>
#include <algorithm>


using namespace std;




// Node object constructor for convenience
avl_node::avl_node (KEYTYPE N): id_num(N) { C[0]=0; C[1]=0; }



avl_tree::avl_tree (): root(0) {} // Initialize root to NULL (for consistency)



avl_tree::~avl_tree () { destroy (root); } // Delete all the node--wrapper

// Recursive helper for destructor
void avl_tree::destroy (avl_node *& curr) {
	if (!curr) return;
	destroy (curr->C[0]);
	destroy (curr->C[1]);
	delete curr;
}



// Return node with matching id_num--wrapper
avl_node* avl_tree::fetch (KEYTYPE I) const { return fetch (root, I); }

// Recursive helper for the function above
avl_node* avl_tree::fetch (avl_node *curr, KEYTYPE I) const {
	if (!curr) return 0;
	if (I == curr->id_num) return curr;
	if (I < curr->id_num) return fetch(curr->C[0], I);
	return fetch(curr->C[1], I);
}



int avl_tree::height () const { return height (root); }

int avl_tree::height (const avl_node *curr) const {
	if (!curr) return 0;
	return 1 + max(height(curr->C[0]),height(curr->C[1]));
}



inline void avl_tree::rotate (avl_node *&curr, bool side) {
	avl_node *temp = curr->C[!side];
	curr->C[!side] = temp->C[side];
	temp->C[side] = curr;
	curr = temp;
}



// Generic BST insertion method--wrapper
void avl_tree::insert (KEYTYPE N) {
	avl_node *temp = new avl_node (N);
	insert(root,temp, 0);
}

// Generic BST insertion method--recursive helper
int avl_tree::insert (avl_node *&curr, avl_node *New, int parent) {
	int val, balfac, cbalfac;
	avl_node *child;

	// only insert at leaf; traversing to NULL means inserting
	if (!curr) { curr = New; return parent; }
	
	// traverse with the good ol' BST fashion
	if (New->id_num < curr->id_num)
		val = insert(curr->C[0],New,-1);
	else 
		val = insert(curr->C[1],New, 1);

	balfac = -height(curr->C[0]) +height(curr->C[1]);
	if (abs(balfac) > 1) { // decide if we need rotation
		if (balfac < 0) { // and if so, decide which way
			child = curr->C[0];
			cbalfac= -height(child->C[0]) +height(child->C[1]);
			if (cbalfac < 0) case_ll (curr);
			else case_lr (curr);
		} else {
			child = curr->C[1];
			cbalfac= -height(child->C[0]) +height(child->C[1]);
			if (cbalfac < 0) case_rl (curr);
			else case_rr (curr);
		}
	}
	return val;
}



// some arbitrary numbers
#define REMOVAL 879345 // node removal
#define NOTHERE 344112 // not found
#define SUCCESS 950608 // success

// Removal method--wrapper
bool avl_tree::remove (KEYTYPE I)
{ return (remove ( root, I, 0, 0 ,0,0) == SUCCESS ); }

// Removal method--recursive helper
int avl_tree::remove (avl_node *&curr, KEYTYPE I, int parent,
			avl_node *found, bool F, bool T) {
	int val = 0;
       	int balfac, cbalfac;
	avl_node *temp, *child;

	if (!curr) { // base cases
		if (!found)
			return NOTHERE; // match not found
		else 	return REMOVAL; // replacement found
		// delete node replacement on return
	}

	// check for what we're finding (match or replacement)h
	if (!found) { // finding replacement if matching node is found
		if (I == curr->id_num) { // if match is found
			if (curr->C[1] || curr->C[0]) {
			// continue traversing for replacement
				T = (curr->C[1]); // direction of traversal
				val=remove(curr->C[T],I,parent, curr ,1,T);
			} else { // this means match is on leaf
				found = curr;
				val = REMOVAL;
			}
		} // now for the normal traversal in finding match
		else if (I < curr->id_num)
			val = remove (curr->C[0], I, -1, 0 ,0,0);
		else	val = remove (curr->C[1], I,  1, 0 ,0,0);
	} // this allows fall-thru, which happens when match == replacement
	if (found) { // (i.e. match is found on leaf node)
		// traverse til null
		if (F) T = !T;
		val = remove (curr->C[T],I,T,found ,0,T);
		if (val == REMOVAL) {
			// replacement found, replacing data to match
			// then delete replacement
			found->id_num = curr->id_num;
			temp = curr; curr = curr->C[!T];
			delete temp; return SUCCESS;
		}
	} else if (val == NOTHERE) return val;

	// compute balfac and decide if rotation is needed
	balfac = -height(curr->C[0]) +height(curr->C[1]);
	if (abs(balfac) > 1) {
		if (balfac < 0) {
			child = curr->C[0];
			cbalfac= -height(child->C[0]) +height(child->C[1]);
			if (cbalfac < 0) case_ll (curr);
			else case_lr (curr);
		} else {
			child = curr->C[1];
			cbalfac= -height(child->C[0]) +height(child->C[1]);
			if (cbalfac < 0) case_rl (curr);
			else case_rr (curr);
		}
	}
	return val;
}



// C on L, New on L: R-rot on curr
inline void avl_tree::case_ll (avl_node *&curr) { rotate (curr, 1); }

// C on L, New on R: L-rot on C, R-rot on curr
inline void avl_tree::case_lr (avl_node *&curr)
	{rotate(curr->C[0],0); rotate(curr,1);}

// C on R, New on L: R-rot on C, L-rot on curr
inline void avl_tree::case_rl (avl_node *&curr)
	{rotate(curr->C[1],1); rotate(curr,0);}

// C on R, New on R: L-rot on curr
inline void avl_tree::case_rr (avl_node *&curr) { rotate (curr, 0); }




