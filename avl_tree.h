// A Simple AVL Tree Implementation
// by Timothy K Handojo
/* Description:
 * This is a barebone implementation of Self-Balancing Binary Tree.
 * This base class is meant to be employed into specialized objects
 * appropriately when needed.
 */


#ifndef KEYTYPE
	#define KEYTYPE unsigned int
#endif



struct avl_node {
	KEYTYPE id_num;
	avl_node *C[2];
	
	avl_node(KEYTYPE N=0);
	virtual ~avl_node() {}
};



class avl_tree {
protected:
	avl_node *root;
public:
	avl_tree();
	~avl_tree();
	avl_node* fetch (KEYTYPE) const;
	void insert (KEYTYPE);
	bool remove (KEYTYPE);
	int height () const;
private:
	void destroy(avl_node*&);
	avl_node *fetch (avl_node*,KEYTYPE) const;
	int height (const avl_node*curr) const;
	inline void rotate (avl_node*&, bool side);

	int insert (avl_node*&,avl_node*,int);
	inline int ins_sort (avl_node*&, avl_node*,int);

	int remove (avl_node*&,KEYTYPE,int,avl_node*,bool,bool);
	
	inline void case_ll (avl_node*&);
	inline void case_lr (avl_node*&);
	inline void case_rl (avl_node*&);
	inline void case_rr (avl_node*&);
};
